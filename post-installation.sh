#! /bin/bash
# Script to automate the post-installation configuration of the OpenShift Cluster
# author: nerdilinchen@gmail.com

cmd_oc="oc"

#Create a ldap identity Provider
echo "Create the LDAP secret"
echo "Enter password"
read x
${cmd_oc} create secret generic ldap-secret --from-literal=bindPassword="$x" -n openshift-config

#Create ConfigMap with the Certificate
if [ "$(ls ca.crt)" ]; then
	echo "Create ConfigMap with LDAP Certificate"
	${cmd_oc} create configmap ca-config-map --from-file=ca.crt=ca.crt -n openshift-config
else
	echo "No LDAP Certificate found"
fi

#Create the identity provider
echo "Check for LDAP-configuration file in directory"
wget https://gitlab.com/IlonkaO/test/raw/master/ldap-config.yaml
if [ $(echo ${?}) -gt 0 ]; then
       echo "ldap-config.yaml is missing. Abort!"
       exit 1
else
       echo "Create LDAP identity provider"	
       ${cmd_oc} apply -f ldap-config.yaml
fi


